import os
import glob
os.chdir('/app_face_recognition/app_age_gender')
from age_gender import image_detect

os.chdir('/app_face_recognition/images/ip_video/')
import face_recognition
import cv2
import numpy as np
import sqlite3
import pandas as pd
import time
import dlib
# This is a demo of running face recognition on a video file and saving the results to a new video file.
#
# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.


# initialise variables

database = "/app_face_recognition/database/video_metadata.db"
# add faces to the know faces list
table_name_kf = 'known_faces'
tablename_res = 'video_logs_fd'

# Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn


def read_table(conn, table_name_kf, database):

    # picks up the data from the csv and inserts it into a panda dataframe
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM ''' + table_name_kf)
    table_list = cursor.fetchall()
    table_df = pd.DataFrame(table_list)
    conn.close()

    return table_df


def insert_results(fr_res, tablename_res):

    conn = create_or_open_db(database)

    # insert data to sql
    fr_res.to_sql(tablename_res, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


# Function 3) Insert guest face encoding into df
def insert_guest(name, face_encoding):

    conn = create_or_open_db(database)
    df_known_faces = pd.DataFrame(columns = ['PersonName','pic_path','image_encoding'])

    df_known_faces.loc[0,'PersonName'] = name
    df_known_faces.loc[0,'pic_path'] = "path/"+name
    df_known_faces.loc[0,'image_encoding'] = str(face_encoding)

    # insert data to sql
    df_known_faces.to_sql(table_name_kf, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()



def face_rec_aeg():

    video_out = True
    guest_count = 0
    size = (1280, 720)

    # get known faces encoding
    conn = create_or_open_db(database)
    known_faces_df = read_table(conn, table_name_kf, database)
    face = known_faces_df[2][0]


    # parse face encoding
    known_faces_df[2] = known_faces_df[2].apply(lambda x:
                               np.fromstring(
                               x.replace('\n','')
                                .replace('[','')
                                .replace(']','')
                                .replace('  ',' '), sep=' '))

    known_faces = known_faces_df[2].tolist()
    face_labels = known_faces_df[0].tolist()

    # Create an output movie file (make sure resolution/frame rate matches input video!)
    if video_out:
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        output_movie = cv2.VideoWriter('/app_face_recognition/videos/2017-12-22/output/cam1_2017-12-22_fr.avi', fourcc, 10, size)

    for file in glob.glob("/app_face_recognition/videos/2017-12-22/cam1_*.mp4"):
        print(file)

        # append logs to dataframe
        fr_result_all = pd.DataFrame()

        fr_result = pd.DataFrame({'ObjectDetected':[],'FrameNumber':[],'NumObjects':[],
                                'FrameObjectNum':[],'Label':[],'Confidence':[],'BottomRight_x':[],
                                'BottomRight_y':[],'TopLeft_x':[],'TopLeft_y':[], 'Age':[], 'Gender':[]})

        # get file names and loop through each file name
        input_movie_path = file
        input_movie_file_name = os.path.basename(input_movie_path)

        #if input_movie_file_name == 'cam1_2017-12-22_0_01_40.mp4' or input_movie_file_name == 'cam1_2017-12-22_0_01_50.mp4':


        input_movie = cv2.VideoCapture(input_movie_path)
        rotate = False
        length = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        width = input_movie.get(3)  # float
        height = input_movie.get(4) # float

        # Initialize some variables
        face_locations = []
        face_encodings = []
        face_names = []
        dlib_face_loc_tuple = ()
        frame_number = 0
        img_size = 64


        # for face detection
        detector = dlib.get_frontal_face_detector()

        while True:

                # Grab a single frame of video
                ret, frame = input_movie.read()

                # Quit when the input video file ends
                if not ret:
                    break

                frame_resize = frame
                frame_resize = cv2.resize(frame, size,  interpolation = cv2.INTER_AREA) #cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
                frame_number += 1
                dlib_face_locations = []

                # rotate image
                if rotate:
                    num_rows, num_cols = frame_resize.shape[:2]
                    rotation_matrix = cv2.getRotationMatrix2D((num_cols/2, num_rows/2), -180, 1)
                    frame_rotate = cv2.warpAffine(frame_resize, rotation_matrix, (num_cols, num_rows))

                else:
                    frame_rotate = frame_resize


                #<<<<<<<<<<<<<<<<<<<<< face locations using dlib >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                input_img = cv2.cvtColor(frame_rotate, cv2.COLOR_BGR2RGB)
                img_h, img_w, _ = np.shape(input_img)

                #detect faces using dlib detector
                detected = detector(input_img, 1)
                faces = np.empty((len(detected), img_size, img_size, 3))

                for i, d in enumerate(detected):
                    x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()

                    # generate facelocatios for recognition
                    dlib_face_loc_tuple = (y1, x2, y2, x1)
                    dlib_face_locations.append(dlib_face_loc_tuple)


                    #print ("Dlib coordinates {}".format(dlib_face_locations))
                    #generate faces for age gender detection
                    xw1 = max(int(x1 - 0.4 * w), 0)
                    yw1 = max(int(y1 - 0.4 * h), 0)
                    xw2 = min(int(x2 + 0.4 * w), img_w - 1)
                    yw2 = min(int(y2 + 0.4 * h), img_h - 1)
                    cv2.rectangle(frame_rotate, (x1, y1), (x2, y2), (255, 0, 0), 2)
                    faces[i,:,:,:] = cv2.resize(frame_rotate[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))


                # Find all the faces and face encodings in the current frame of video
                #face_locations = face_recognition.face_locations(frame_rotate)

                if len(dlib_face_locations)>=1:
                    face_encodings = face_recognition.face_encodings(frame_rotate, dlib_face_locations)

                    face_names = []
                    face_similarity = []

                    #<<<<<<<<<<<<<<<<<<<<<<< Face Recognition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    for face_encoding in face_encodings:
                        # See if the face is a match for the known face(s)

                        distance_score = face_recognition.face_distance(known_faces, face_encoding)
                        face_index = np.argmin(distance_score)#[-1:0]

                        if distance_score[face_index]<0.45:
                                name = face_labels[face_index]
                                face_similarity_score = round((1 - distance_score[face_index]), 2)

                                #update identified names to a list
                                face_names.append(name)
                                face_similarity.append(face_similarity_score)



                                # print(name)

                        elif distance_score[face_index]>0.75:
                                #time_now = str(int(time.time()))[-3:]
                                guest_count = guest_count + 1
                                print (guest_count)
                                name = "Guest " + str((guest_count))
                                face_similarity_score = 0

                                # insert the guest name into the sql db
                                insert_guest(name, face_encoding)

                                #append the face encoding and label to the list
                                known_faces.append(face_encoding)
                                face_labels.append(name)

                                #update identified names to a list
                                face_names.append(name)
                                face_similarity.append(face_similarity_score)




                        #match = face_recognition.compare_faces(known_faces, face_encoding, tolerance=0.50)



                    #<<<<<<<<<<<<<<<<<<<<<<< Age and Gender detection >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    age_gender_list = image_detect(faces, detected)

                    #name_age_gender = str(name) + str(age_gender_label)

                    # Label the results
                    for (top, right, bottom, left), name, similarity, age_gender in zip(dlib_face_locations, face_names, face_similarity, age_gender_list):

                        if video_out:

                            out_label = name+"("+str(similarity)+")"+"- "+str(int(age_gender[0]))+" "+age_gender[1]

                            if not name:
                                continue

                            # Draw a box around the face
                            cv2.rectangle(frame_rotate, (left, top), (right, bottom), (0, 0, 255), 2)

                            # Draw a label with a name below the face
                            cv2.rectangle(frame_rotate, (left, bottom - 10), (right, bottom), (0, 0, 255), cv2.FILLED)
                            font = cv2.FONT_HERSHEY_DUPLEX
                            cv2.putText(frame_rotate, out_label, (left + 3, bottom - 3), font, 0.5, (255, 255, 255), 1)


                        frame_result = {'ObjectDetected': 1,
                                        'FrameNumber':frame_number,
                                        'NumObjects':1,
                                        'FrameObjectNum':0,
                                        'Label':name,
                                        'Confidence':similarity,
                                        'BottomRight_x':right,
                                        'BottomRight_y':bottom,
                                        'TopLeft_x':left,
                                        'TopLeft_y':top,
                                        'Age':int(age_gender[0]),
                                        'Gender':age_gender[1],
                                        }


                        fr_result = fr_result.append(frame_result, ignore_index=True)

                        if not name:
                            continue


                # Write the resulting image to the output video file
                if video_out:
                    output_movie.write(frame_rotate)
                print("Writing frame {} / {}".format(frame_number, length))


        # file metadata to insert into sql
        file_metadata = input_movie_file_name.split('_')

        camera_name = file_metadata[0]
        file_date = file_metadata[1]
        file_hour = int(file_metadata[2])
        file_min = int(file_metadata[3])
        file_sec = int(file_metadata[4][:2])


        fr_result['FrameWidth']  = width
        fr_result['FrameHeight'] = height
        fr_result['CameraName'] = camera_name
        fr_result['Date'] = file_date
        fr_result['Hour'] = file_hour
        fr_result['Minute'] = file_min
        fr_result['Second'] = file_sec

        fr_result_all = fr_result_all.append(fr_result, ignore_index=True)

        # insert face recognition data to sql db
        insert_results(fr_result_all, tablename_res)
        print('Inserted into DB')
    return 0

    # All done!
    input_movie.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
        face_rec_aeg()
