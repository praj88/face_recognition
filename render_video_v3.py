import os
#os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/face_recognition/')

import cv2
import numpy as np
import sqlite3
import pandas as pd
import time

# initialise variables
database_1 = "/app_face_recognition/database/video_metadata.db"
database_2 = "/app_face_recognition/database/video_metadata_20180106.db"
# add faces to the know faces list
table_name_vl = 'video_logs'
table_name_vl_fd = 'video_logs_fd'

# Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn


def read_table(conn, table_name, database):

    # picks up the data from the csv and inserts it into a panda dataframe
    sql_read_q ='''SELECT * FROM ''' + table_name
    table_df =  pd.read_sql(sql_read_q, conn)
    conn.close()

    return table_df

def gen_fileName(video_logs_df_all):

    video_logs_df_all['Second'] = video_logs_df_all.Second.apply(lambda x: str(int(x)).zfill(2))
    video_logs_df_all['Minute'] = video_logs_df_all.Minute.apply(lambda x: str(int(x)).zfill(2))

    video_logs_df_all['filename'] = video_logs_df_all['CameraName'] + "_" + video_logs_df_all['Date']+"_" + video_logs_df_all['Hour'].astype('str')+"_" + video_logs_df_all['Minute'].astype('str')+"_" + video_logs_df_all['Second'].astype('str')

    return video_logs_df_all

def resize_video(video_path):

    input_movie = cv2.VideoCapture(video_path)
    length_resize = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_number = 0
    re_size = (640,360)

    # Create an output movie file (make sure resolution/frame rate matches input video!)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    output_movie_resize = cv2.VideoWriter('/app_face_recognition/videos/2018-01-01/output/cam_2018-01-01_all_resized_3.avi', fourcc, 30, re_size)

    while True:



            # Grab a single frame of video
            ret, frame = input_movie.read()

            # Quit when the input video file ends
            if not ret:
                break

            frame = cv2.resize(frame, re_size,  interpolation = cv2.INTER_AREA)

            frame_number += 1
            # Write the resulting image to the output video file
            print("Writing frame {} / {}".format(frame_number, length_resize))
            if frame_number>=2000: # and frame_number<2000:
                output_movie_resize.write(frame)


def render_video():

    # get known faces encoding
    conn = create_or_open_db(database_2)
    video_logs_df = read_table(conn, table_name_vl, database_2)
    conn = create_or_open_db(database_1)
    video_logs_fd_df = read_table(conn, table_name_vl_fd, database_1)
    video_logs_df_all = video_logs_df.append(video_logs_fd_df)
    size = (1280, 720)
    # generate file name
    video_logs_df_filename = gen_fileName(video_logs_df_all)

    #filter for rows for the selected video file
    video_logs_df_filename = video_logs_df_filename[video_logs_df_filename['filename'].str.contains("2018")]
    video_logs_df_grouped = video_logs_df_filename.groupby('filename')

    # Create an output movie file (make sure resolution/frame rate matches input video!)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    output_movie = cv2.VideoWriter('/app_face_recognition/videos/2018-01-01/output/cam_2018-01-01_all.avi', fourcc, 30, size)


    for filename, group in video_logs_df_grouped:
        video_file = group.filename.unique()[0]
        print((video_file))
        # Open the input movie file

        input_movie = cv2.VideoCapture("/app_face_recognition/videos/2018-01-01/" + video_file + ".mp4")

        length = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))
        #print(length)
        # width and height of the file
        width = input_movie.get(3)  # float
        height = input_movie.get(4) # float

        frame_number = 0
        df_iter = 0

        while True:

            # Grab a single frame of video
            ret, frame = input_movie.read()

            # Quit when the input video file ends
            if not ret:
                break

            frame = cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)

            video_logs_sorted_df = group.sort_values(by='FrameNumber', ascending=1, inplace=False)
            video_logs_indexed_df = video_logs_sorted_df.reset_index( drop=True)
            length_df = len(video_logs_indexed_df)

            if df_iter<length_df:


                if video_logs_indexed_df.loc[df_iter, 'FrameNumber'] == frame_number:
                    loop_frame = True
                    while loop_frame:
                        print (video_logs_indexed_df.loc[df_iter,'Label'])
                        right = int(video_logs_indexed_df.loc[df_iter, 'BottomRight_x'])
                        bottom = int(video_logs_indexed_df.loc[df_iter, 'BottomRight_y'])
                        left = int(video_logs_indexed_df.loc[df_iter, 'TopLeft_x'])
                        top = int(video_logs_indexed_df.loc[df_iter, 'TopLeft_y'])
                        label = video_logs_indexed_df.loc[df_iter,'Label']
                        confidence = video_logs_indexed_df.loc[df_iter, 'Confidence']

                        output_label = label + " (" + str(confidence) + ")"



                        font = cv2.FONT_HERSHEY_DUPLEX

                        if ("person" in output_label) or ("Prajwal" in output_label) or ("Guest" in output_label) or ("Anusha" in output_label):

                            # Draw a box around the face
                            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                            # Draw a label with a name below the face
                            cv2.rectangle(frame, (left, bottom - 10), (right, bottom), (0, 0, 255), cv2.FILLED)

                            cv2.putText(frame, output_label, (left + 3, top- 3), font, 0.5, (255, 255, 255), 1)
                        else:

                            # Draw a box around the face
                            cv2.rectangle(frame, (left, top), (right, bottom), (255, 0, 0), 2)
                            # Draw a label with a name below the face
                            cv2.rectangle(frame, (left, bottom - 10), (right, bottom), (255, 0, 0), cv2.FILLED)
                            cv2.putText(frame, output_label, (left + 3, top- 3), font, 0.5, (255, 0, 0), 1)


                        loop_frame = False

                        # increase count of iterator
                        df_iter_next = df_iter + 1
                        if df_iter_next<length_df:
                            if video_logs_indexed_df.loc[df_iter, 'FrameNumber']  == video_logs_indexed_df.loc[df_iter_next, 'FrameNumber']:
                                loop_frame = True

                        df_iter = df_iter + 1


            frame_number += 1
            # Write the resulting image to the output video file
            print("Writing frame {} / {}".format(frame_number, length))
            output_movie.write(frame)



if __name__ == '__main__':
    render_video()

    # Resize the video if the file size is too large
    resize_video('/app_face_recognition/videos/2018-01-01/output/cam_2018-01-01_all.avi')
