import os
os.chdir('/app_face_recognition/app_age_gender')
from age_gender import image_detect

#os.chdir('/app_face_recognition/images/ip_video/')
#import face_recognition
import cv2
import numpy as np
#import sqlite3
import pandas as pd
import time
import dlib

import tensorflow as tf
from scipy import misc
from os.path import join as pjoin
import sys
import copy
import math
import pickle

os.chdir('/app_face_recognition/facenet/facenet-master/src')
import facenet
import align.detect_face as detect_face

# This is a demo of running face recognition on a video file and saving the results to a new video file.

# initialise variables
database = "/app_face_recognition/database/video_metadata.db"
# add faces to the know faces list
table_name_kf = 'known_faces'
tablename_res = 'video_logs_fd'



# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Load Face net parameters >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
print('Creating networks and loading parameters')
with tf.Graph().as_default():
    #gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    #sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
    sess = tf.Session()
    with sess.as_default():
        pnet, rnet, onet = detect_face.create_mtcnn(sess, '/app_face_recognition/facenet/facenet-master/src/align/')

        minsize = 20 # minimum size of face
        threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
        factor = 0.709 # scale factor
        margin = 44

        baseImage_interval = 1
        batch_size = 1
        image_size = 182
        input_image_size = 160
        size = (800, 450)
        #size = (800, 450)

        HumanNames = ['Anusha','Prajwal','Ujwal']    #train human name

        print('Loading feature extraction model')
        modeldir = '/app_face_recognition/facenet/facenet-master/src/models/20170512-110547/20170512-110547.pb'
        facenet.load_model(modeldir)

        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]


        classifier_filename = '/app_face_recognition/facenet/facenet-master/src/my_classifier/my_classifier.pkl'
        classifier_filename_exp = os.path.expanduser(classifier_filename)
        with open(classifier_filename_exp, 'rb') as infile:
            (model, class_names) = pickle.load(infile)
            print('load classifier file-> %s' % classifier_filename_exp)


# output debug
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#output_movie = cv2.VideoWriter('/app_face_recognition/videos/2017-12-22/output/cam1_2017-12-22_out_fn_debug.avi', fourcc, 5, size)

# Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn


def read_table(conn, table_name_kf, database):

    # picks up the data from the csv and inserts it into a panda databaseImage
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM ''' + table_name_kf)
    table_list = cursor.fetchall()
    table_df = pd.DatabaseImage(table_list)
    conn.close()

    return table_df


def insert_results(fr_res, tablename_res):

    conn = create_or_open_db(database)

    # insert data to sql
    fr_res.to_sql(tablename_res, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


# Function 3) Insert guest face encoding into df
def insert_guest(name, face_encoding):

    conn = create_or_open_db(database)
    df_known_faces = pd.DatabaseImage(columns = ['PersonName','pic_path','image_encoding'])

    df_known_faces.loc[0,'PersonName'] = name
    df_known_faces.loc[0,'pic_path'] = "path/"+name
    df_known_faces.loc[0,'image_encoding'] = str(face_encoding)

    # insert data to sql
    df_known_faces.to_sql(table_name_kf, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


def get_faceRec(x, y, w, h, img_h, img_w, baseImage, fid):

    fr_out_name_all = {}
    fr_out_confidence_all = {}
    dlib_face_locations = []
    img_size = 64
    baseImage_interval = 1
    face_names = []
    face_similarity = []
    fr_flag = {}
    rec_flag = False
    # holds faces for the age detection functions
    faces = np.empty((1, img_size, img_size, 3))

    x1 = x
    y1 = y

    x2 = x + w
    y2 = y + h

    dlib_face_locations = [x1, y1, x2, y2]
    #print ("Dlib coordinates {}".format(dlib_face_locations))
    #generate faces for age gender detection
    xw1 = max(int(x1 - 0.4 * w), 0)
    yw1 = max(int(y1 - 0.4 * h), 0)
    xw2 = min(int(x2 + 0.4 * w), img_w - 1)
    yw2 = min(int(y2 + 0.4 * h), img_h - 1)
    faces[0,:,:,:] = cv2.resize(baseImage[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

    # generate facelocations for recognition
    dlib_face_loc_tuple = (x1, y1, x2, y2)


    #print (dlib_face_locations)
    #print ("x1:{}, y1:{}, x2:{}, y2:{}, w:{}, h:{}".format(x1, y1, x2, y2, w, h))

    bounding_boxes = dlib_face_locations


    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Facenet implementation >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    det = bounding_boxes
    #print ("det Box {}".format(det))
    cropped = []
    scaled = []
    scaled_reshape = []
    inner_flag = False
    bb = np.zeros((1,4), dtype=np.int32)
    inner_count = 0
    j = 0
    i = 0
    #for i in range(nrof_faces):
    emb_array = np.zeros((1, embedding_size))

    bb[i][0] = det[0]
    bb[i][1] = det[1]
    bb[i][2] = det[2]
    bb[i][3] = det[3]

    #print ("Bounded Box {}".format(bb))
    # inner exception
    if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= size[0] or bb[i][3] >= size[1]:
        print('face is inner of range!')
        fr_out_name_all[fid] = ""
        fr_out_confidence_all[fid] = 0

        rec_flag = False
        fr_flag[fid] = rec_flag

    else:
        cropped.append(baseImage[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
        #print ("Inner Range i:{}, cropped:{}".format(i, len(cropped)))
        cropped[j] = facenet.flip(cropped[j], False)
        scaled.append(misc.imresize(cropped[j], (image_size, image_size), interp='bilinear'))
        scaled[j] = cv2.resize(scaled[j], (input_image_size,input_image_size), interpolation=cv2.INTER_CUBIC)
        scaled[j] = facenet.prewhiten(scaled[j])
        scaled_reshape.append(scaled[j].reshape(-1,input_image_size,input_image_size,3))

        #print ("Input feed Dict {}".format(scaled_reshape[i]))
        feed_dict = {images_placeholder: scaled_reshape[j], phase_train_placeholder: False}
        emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
        predictions = model.predict_proba(emb_array)
        best_class_indices = np.argmax(predictions, axis=1)
        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
        cv2.rectangle(baseImage, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)    #boxing face
        confidence = np.around((best_class_probabilities[0]),2)

        #plot result idx under box
        text_x = bb[i][0]
        text_y = bb[i][3] + 20

        # print('result: ', best_class_indices[0])

        confidence_str = str(confidence)
        #print ("Face No {}:{}".format(i , confidence_str))
        for H_i in HumanNames:
            #print("Identified Faces {} / {}".format(H_i, HumanNames[best_class_indices[0]]))
            if HumanNames[best_class_indices[0]] == H_i:
                if (confidence)>0.875:
                    rec_flag = True
                    result_names = HumanNames[best_class_indices[0]]
                    output_label = str(result_names +" ("+confidence_str+")")
                    face_names.append(result_names)
                    face_similarity.append(confidence_str)

                    fr_out_name = result_names
                    fr_out_confidence = confidence_str

                    fr_out_name_all[fid] = fr_out_name
                    fr_out_confidence_all[fid] = fr_out_confidence

                    fr_flag[fid] = rec_flag
                    cv2.putText(baseImage, output_label, (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                               1, (0, 0, 255), thickness=1, lineType=2)

                    print (fr_out_name_all)
                    print (fr_flag[fid])

                elif (confidence)<0.3:
                    #time_now = str(int(time.time()))[-3:]
                    #guest_count = guest_count + 1
                    #print (guest_count)
                    result_names = "Guest"# + str((guest_count))
                    rec_flag = True
                    face_names.append(result_names)
                    face_similarity.append(confidence_str)

                    fr_out_name = result_names
                    fr_out_confidence = confidence_str

                    fr_out_name_all[fid] = fr_out_name
                    fr_out_confidence_all[fid] = fr_out_confidence

                    fr_flag[fid] = rec_flag
                    cv2.putText(baseImage, output_label, (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                               1, (0, 0, 255), thickness=1, lineType=2)
                else:
                    rec_flag = False

                    fr_out_name = "Detecting"
                    fr_out_confidence = '0'

                    fr_out_name_all[fid] = fr_out_name
                    fr_out_confidence_all[fid] = fr_out_confidence

                    fr_flag[fid] = rec_flag

    #output_movie.write(baseImage)



    # if nrof_faces>0:

        #<<<<<<<<<<<<<<<<<<<<<<< Age and Gender detection >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        # age_gender_list = image_detect(faces, detected)
        #
        # #name_age_gender = str(name) + str(age_gender_label)
        #
        # # Label the results
        # for (top, right, bottom, left), name, similarity, age_gender in zip(dlib_face_locations, face_names, face_similarity, age_gender_list):
        #     print("Length of faces {} and names {}".format(len(dlib_face_locations), len(face_names)))
        #     fr_out = name+"("+str(similarity)+")"+"- "+str(int(age_gender[0]))+" "+age_gender[1]
        #     fr_out_all[ fid ] = fr_out


    return fr_out_name_all, fr_out_confidence_all, fr_flag
