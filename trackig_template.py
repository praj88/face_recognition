#Import the OpenCV and dlib libraries
import cv2
import dlib

import numpy as np
import threading
import time


#Initialize a face cascade using the frontal face haar cascade provided with
#the OpenCV library
#Make sure that you copy this file from the opencv project to the root of this
#project folder
def get_Rec(..., tid):




def detectAndTrackMultipleItems():

    #size = (1920, 1080)
    size = (1280, 720)

    #Input vide stream
    capture = cv2.VideoCapture('path/to/inputvideo')
    length = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))

    # Create an output movie file (make sure resolution/frame rate matches input video!)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    output_movie = cv2.VideoWriter('path/to/Outputvideo', fourcc, 10, size)

    #The color of the rectangle we draw around the face
    rectangleColor = (0,165,255)

    #variables holding the current frame number and the current faceid
    frameCounter = 0
    currentTrackID = 0

    #Variables holding the correlation trackers and the name per faceid
    trackers = {}
    faceNames = {}

    try:
        while True:
            #Retrieve the latest image
            ret, fullSizeBaseImage = capture.read()

                # Quit when the input video file ends
                if not ret:
                    break

            #Resize the image to 320x240
            baseImage = cv2.resize(fullSizeBaseImage, size)

            #Result image is the image we will show the user, which is a
            #combination of the original image from the webcam and the
            #overlayed rectangle for the largest face
            resultImage = baseImage.copy()



            #STEPS:
            # * Update all trackers and remove the ones that are not
            #   relevant anymore
            # * Every 10 frames:
            #       + Use  detection on the current frame and look
            #         for new persons.
            #       + For each found person, check if centerpoint is within
            #         existing tracked box. If so, nothing to do
            #       + If centerpoint is NOT in existing tracked box, then
            #         we add a new tracker with a new track-id


            #Increase the framecounter
            frameCounter += 1



            #Update all the trackers and remove the ones for which the update
            #indicated the quality was not good enough
            tidsToDelete = []
            for tid in trackers.keys():
                trackingQuality = trackers[ tid ].update( baseImage )

                #If the tracking quality is good enough, we must delete
                #this tracker
                #The number for track quality can be changed as a parameter, higher the number better quality of tracking but reduces duration of each tracker
                if trackingQuality < 5:
                    tidsToDelete.append( tid )

            for tid in tidsToDelete:
                print("Removing tid " + str(tid) + " from list of trackers")
                trackers.pop( tid , None )




            #Every 10 frames, we will have to determine which faces
            #are present in the frame
            if (frameCounter % 5) == 0:



                #For the face detection, we need to make use of a gray
                #colored image so we will convert the baseImage to a
                #gray-based image
                gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
                #Now use the haar cascade detector to find all faces
                #in the image
                #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Add Detection Functions >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                objects_detected = object_detection(baseImage)


                #Loop over all faces and check if the area for this
                #face is the largest so far
                #We need to convert it to int here because of the
                #requirement of the dlib tracker. If we omit the cast to
                #int here, you will get cast errors since the detector
                #returns numpy.int32 and the tracker requires an int
                for (_x,_y,_w,_h) in objects_detected: # faces:
                    x = int(_x)
                    y = int(_y)
                    w = int(_w)
                    h = int(_h)


                    #calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h



                    #Variable holding information which faceid we
                    #matched with
                    matchedtid = None

                    #Now loop over all the trackers and check if the
                    #centerpoint of the face is within the box of a
                    #tracker

                    for tid in trackers.keys():
                        tracked_position =  trackers[tid].get_position()

                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())


                        #calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        #check if the centerpoint of the face is within the
                        #rectangleof a tracker region. Also, the centerpoint
                        #of the tracker region must be within the region
                        #detected as a face. If both of these conditions hold
                        #we have a match
                        if ( ( t_x <= x_bar   <= (t_x + t_w)) and
                             ( t_y <= y_bar   <= (t_y + t_h)) and
                             ( x   <= t_x_bar <= (x   + w  )) and
                             ( y   <= t_y_bar <= (y   + h  ))):
                            matchedtid = tid


                    #If no matched tid, then we have to create a new tracker
                    if matchedtid is None:

                        print("Creating new tracker " + str(currentTrackID))

                        #Create and store the tracker
                        tracker = dlib.correlation_tracker()
                        tracker.start_track(baseImage,
                                            dlib.rectangle( x-5,
                                                            y-10,
                                                            x+w+5,
                                                            y+h+10))

                        trackers[ currentTrackID ] = tracker

                        #Start face recognition.
                        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Add Recognition Function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        out_all = get_Rec(..., currentTrackID)

                        #Increase the currentFaceID counter
                        currentTrackID += 1

            #Now loop over all the trackers we have and draw the rectangle
            #around the detected faces. If we 'know' the name for this person
            #(i.e. the recognition thread is finished), we print the name
            #of the person, otherwise the message indicating we are detecting
            #the name of the person
            for tid in trackers.keys():
                tracked_position =  trackers[tid].get_position()

                t_x = int(tracked_position.left())
                t_y = int(tracked_position.top())
                t_w = int(tracked_position.width())
                t_h = int(tracked_position.height())

                cv2.rectangle(resultImage, (t_x, t_y),
                                        (t_x + t_w , t_y + t_h),
                                        rectangleColor ,2)


                if tid in fr_out_all.keys():
                #if label_flag:
                    cv2.putText(resultImage, out_all[tid] ,
                                (int(t_x + t_w/2), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, (255, 255, 255), 2)
                else:
                   cv2.putText(resultImage, "Detecting..." ,
                               (int(t_x + t_w/2), int(t_y)),
                               cv2.FONT_HERSHEY_SIMPLEX,
                               0.5, (255, 255, 255), 2)




            #Finally, we want to show the images on the screen
            if video_out:
                output_movie.write(resultImage)
            print("Writing frame {} / {}".format(frameCounter, length))





    #To ensure we can also deal with the user pressing Ctrl-C in the console
    #we have to check for the KeyboardInterrupt exception and break out of
    #the main loop
    except KeyboardInterrupt as e:
        pass

    #Destroy any OpenCV windows and exit the application
    cv2.destroyAllWindows()
    exit(0)


if __name__ == '__main__':
    detectAndTrackMultipleFaces()
