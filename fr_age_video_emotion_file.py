import os
os.chdir('/app_face_recognition/app_age_gender')
from age_gender import image_detect

os.chdir('/app_face_recognition/images/')
import face_recognition
import cv2
import numpy as np
from keras.models import load_model
# This is a demo of running face recognition on a video file and saving the results to a new video file.
#
# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.

#emotion model
emotion_model_path = '/app_face_recognition/emotion_recognition/face_classification-master/trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_model = load_model(emotion_model_path, compile=False)

# Open the input movie file
input_movie = cv2.VideoCapture("prajwal_mov.mp4")
rotate = True
length = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

# Create an output movie file (make sure resolution/frame rate matches input video!)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
output_movie = cv2.VideoWriter('prajwal_fr_age_emotion.avi', fourcc, 29.97, (640, 360))


width = input_movie.get(3)  # float
height = input_movie.get(4) # float

# Load some sample pictures and learn how to recognize them.
lmm_image = face_recognition.load_image_file("prajwal2.jpg")
ps_face_encoding = face_recognition.face_encodings(lmm_image)[0]

al_image = face_recognition.load_image_file("anusha.jpg")
an_face_encoding = face_recognition.face_encodings(al_image)[0]

known_faces = [ps_face_encoding, an_face_encoding]

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
frame_number = 0
size = (640, 360)

while True:
    while frame_number<400:

        # Grab a single frame of video
        ret, frame = input_movie.read()

        frame_resize = frame
        frame_resize = cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)#cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        frame_number += 1

        # rotate image
        if rotate:
            num_rows, num_cols = frame_resize.shape[:2]
            rotation_matrix = cv2.getRotationMatrix2D((num_cols/2, num_rows/2), -180, 1)
            frame_rotate = cv2.warpAffine(frame_resize, rotation_matrix, (num_cols, num_rows))

        else:
            frame_rotate = frame_resize

        # Quit when the input video file ends
        if not ret:
            break

        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(frame_rotate)
        print(type(face_locations))

        if len(face_locations)>=1:
            face_encodings = face_recognition.face_encodings(frame_rotate, face_locations)

            face_names = []

            #<<<<<<<<<<<<<<<<<<<<<<< Face Recognition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                match = face_recognition.compare_faces(known_faces, face_encoding, tolerance=0.50)

                # If you had more than 2 faces, you could make this logic a lot prettier
                # but I kept it simple for the demo
                name = None
                if match[0]:
                    name = "Prajwal"
                elif match[1]:
                    name = "Anusha"

                face_names.append(name)


            #<<<<<<<<<<<<<<<<<<<<<<< Age and Gender detection >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            age_gender_label = image_detect(frame_rotate)
            print (age_gender_label)
            name_age_gender = str(name) + str(age_gender_label)

            # Label the results
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                if not name:
                    continue

                # Draw a box around the face
                cv2.rectangle(frame_rotate, (left, top), (right, bottom), (0, 0, 255), 2)

                # Draw a label with a name below the face
                cv2.rectangle(frame_rotate, (left, bottom - 25), (right, bottom), (0, 0, 255), cv2.FILLED)
                font = cv2.FONT_HERSHEY_DUPLEX
                cv2.putText(frame_rotate, name_age_gender, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)

            #<<<<<<<<<<<<<<<<<<<<<<< emotion detection >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            x1 = face_locations[0][0]
            y1 = face_locations[0][1]
            x2 = face_locations[0][2]
            y2 = face_locations[0][3]

            print(x1)
            #emotion_label_arg = np.argmax(emotion_classifier.predict(frame_rotate))
            #emotion_text = emotion_labels[emotion_label_arg]
            #print(emotion_text)

        # Write the resulting image to the output video file
        print("Writing frame {} / {}".format(frame_number, length))
        output_movie.write(frame_rotate)

# All done!
input_movie.release()
cv2.destroyAllWindows()
