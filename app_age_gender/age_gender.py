import os
os.chdir('/app_face_recognition/app_age_gender')
import cv2
import dlib
import numpy as np
import argparse
from wide_resnet import WideResNet
#import face_recognition


#load model weight path
depth = 16 #args.depth
k = 8 #args.width
weight_file = os.path.join("pretrained_models", "weights.18-4.06.hdf5") #args.weight_file

# load model and weights
img_size = 64
model = WideResNet(img_size, depth=depth, k=k)()
model.load_weights(weight_file)

def get_args():
    parser = argparse.ArgumentParser(description="This script detects faces from web cam input, "
                                                 "and estimates age and gender for the detected faces.",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--weight_file", type=str, default=None,
                        help="path to weight file (e.g. weights.18-4.06.hdf5)")
    parser.add_argument("--depth", type=int, default=16,
                        help="depth of network")
    parser.add_argument("--width", type=int, default=8,
                        help="width of network")
    args = parser.parse_args()
    return args


def draw_label(image, point, label, font=cv2.FONT_HERSHEY_SIMPLEX,
               font_scale=1, thickness=2):
    size = cv2.getTextSize(label, font, font_scale, thickness)[0]
    x, y = point
    cv2.rectangle(image, (x, y - size[1]), (x + size[0], y), (255, 0, 0), cv2.FILLED)
    cv2.putText(image, label, point, font, font_scale, (255, 255, 255), thickness)


def get_age_gender(image_path):

    # for face detection
    detector = dlib.get_frontal_face_detector()

    # capture video
    cap = cv2.VideoCapture(image_path) #cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

    while True:
        # get video frame
        ret, img = cap.read()

        #if not ret:
        #    print("error: failed to capture image")
        #    return -1

        input_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_h, img_w, _ = np.shape(input_img)

        # detect faces using dlib detector
        detected = detector(input_img, 1)
        faces = np.empty((len(detected), img_size, img_size, 3))

        for i, d in enumerate(detected):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            xw1 = max(int(x1 - 0.4 * w), 0)
            yw1 = max(int(y1 - 0.4 * h), 0)
            xw2 = min(int(x2 + 0.4 * w), img_w - 1)
            yw2 = min(int(y2 + 0.4 * h), img_h - 1)
            cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
            # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
            faces[i,:,:,:] = cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

        if len(detected) > 0:
            # predict ages and genders of the detected faces
            results = model.predict(faces)
            predicted_genders = results[0]
            ages = np.arange(0, 101).reshape(101, 1)
            predicted_ages = results[1].dot(ages).flatten()
            #print (predicted_ages)
            #print (predicted_genders)

        # draw results
        for i, d in enumerate(detected):
            label = "{}, {}".format(int(predicted_ages[i]),
                                    "F" if predicted_genders[i][0] > 0.5 else "M")
            #draw_label(img, (d.left(), d.top()), label)
            print (label)

        return label

#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Main function to detect age from face >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def image_detect_old(frame):

    '''frame_number = 0

    while True:
        while frame_number<400:

            # Grab a single frame of video
            ret, frame = input_movie.read()
            frame_resize = frame
            frame_resize = cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)#cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            frame_number += 1

            # rotate image
            num_rows, num_cols = frame_resize.shape[:2]
            rotation_matrix = cv2.getRotationMatrix2D((num_cols/2, num_rows/2), -90, 1)
            frame_rotate = cv2.warpAffine(frame_resize, rotation_matrix, (num_cols, num_rows))'''

    # for face detection
    detector = dlib.get_frontal_face_detector()
    input_img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    img_h, img_w, _ = np.shape(input_img)

    # detect faces using dlib detector
    detected = detector(input_img, 1)
    faces = np.empty((len(detected), img_size, img_size, 3))

    for i, d in enumerate(detected):
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        xw1 = max(int(x1 - 0.4 * w), 0)
        yw1 = max(int(y1 - 0.4 * h), 0)
        xw2 = min(int(x2 + 0.4 * w), img_w - 1)
        yw2 = min(int(y2 + 0.4 * h), img_h - 1)
        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
        # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
        faces[i,:,:,:] = cv2.resize(frame[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

    if len(detected) > 0:
        # predict ages and genders of the detected faces
        results = model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()
        print(detected)
        print (predicted_ages)
        print (predicted_genders)

    # parse results
    for i, d in enumerate(detected):
        age_gender_label = "{}, {}".format(int(predicted_ages[i]),
                                "F" if predicted_genders[i][0] > 0.5 else "M")
        #draw_label(frame, (d.left(), d.top()), age_gender_label)
        #print (age_gender_label)

        return age_gender_label

def image_detect_old(frame):

    '''frame_number = 0

    while True:
        while frame_number<400:

            # Grab a single frame of video
            ret, frame = input_movie.read()
            frame_resize = frame
            frame_resize = cv2.resize(frame, size,  interpolation = cv2.INTER_AREA)#cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            frame_number += 1

            # rotate image
            num_rows, num_cols = frame_resize.shape[:2]
            rotation_matrix = cv2.getRotationMatrix2D((num_cols/2, num_rows/2), -90, 1)
            frame_rotate = cv2.warpAffine(frame_resize, rotation_matrix, (num_cols, num_rows))'''

    # for face detection
    detector = dlib.get_frontal_face_detector()
    input_img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    img_h, img_w, _ = np.shape(input_img)

    # detect faces using dlib detector
    detected = detector(input_img, 1)
    faces = np.empty((len(detected), img_size, img_size, 3))

    for i, d in enumerate(detected):
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        xw1 = max(int(x1 - 0.4 * w), 0)
        yw1 = max(int(y1 - 0.4 * h), 0)
        xw2 = min(int(x2 + 0.4 * w), img_w - 1)
        yw2 = min(int(y2 + 0.4 * h), img_h - 1)
        cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
        # cv2.rectangle(img, (xw1, yw1), (xw2, yw2), (255, 0, 0), 2)
        faces[i,:,:,:] = cv2.resize(frame[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))

    if len(detected) > 0:
        # predict ages and genders of the detected faces
        results = model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()
        print(detected)
        print (predicted_ages)
        print (predicted_genders)

    # parse results
    for i, d in enumerate(detected):
        age_gender_label = "{}, {}".format(int(predicted_ages[i]),
                                "F" if predicted_genders[i][0] > 0.5 else "M")
        #draw_label(frame, (d.left(), d.top()), age_gender_label)
        #print (age_gender_label)

        return age_gender_label

def image_detect(faces, detected):

    age_gender_list = []

    if len(detected) > 0:
        # predict ages and genders of the detected faces
        results = model.predict(faces)
        predicted_genders = results[0]

        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

    # parse results
    for i, d in enumerate(detected):
        age_label = predicted_ages[i]
        gender_label = "F" if predicted_genders[i][0] > 0.5 else "M"
        age_gender_list.append([age_label, gender_label])
        #draw_label(frame, (d.left(), d.top()), age_gender_label)

    return age_gender_list


# if __name__ == '__main__':
#     image_path = "/app_face_recognition/images/anusha_me2.jpg"
#     get_age_gender(image_path)
#    image_detect(input_movie)
